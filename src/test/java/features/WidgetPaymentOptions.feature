Feature: Configure Widget payment options
  Verify if user is able to start widget with different payment options selected

  @integration
  Scenario: Reinitialize and start widget with Credi Card and Express Financing
    Given I am on testCdn page
    When I select CreditCard and ExpressFinancing
    Then I should see CreditCard is selected
    Then I should see ExpressFinancing is selected
    When I Re-Initialize Widget
    And I run with Buy Now "tax"
    Then I should see widget started with CreditCard and ExpressFinancing

  @integration
  Scenario: Reinitialize and start widget with eCheck and Express Financing
    Given I am on testCdn page
    When I select ACH and ExpressFinancing
    Then I should see ACH is selected
    Then I should see ExpressFinancing is selected
    When I Re-Initialize Widget
    And I run with Buy Now "tax"
    Then I should see widget started with ACH and ExpressFinancing

  @integration
  Scenario: Reinitialize and start widget with all options
    Given I am on testCdn page
    When I select all payment options
    Then I should see all payment options are selected
    When I Re-Initialize Widget
    And I run with Buy Now "tax"
    Then I should see widget started with all payment options

