Feature: Credit Card Scenarios
  Verify purchase cart by Credit Card and expected success

  @integration
  Scenario Outline: Purchase cart by Credit Card - <scenNbr>
    Given I am on testCdn page
    And I select all payment options
    And I Re-Initialize Widget
    And I run with Buy Now "<type>"
    And I click on CreditCard option
    Then I enter first name as "<firstName>"
    And I enter last name as "<lastName>"
    And I enter card number as "<creditCardNbr>"
    And I select exp month as "<expMonth>"
    And I select exp year as "<expYear>"
    And I enter cvv as "<cvv>"
    And I enter billing address as "<billingAddress>"
    And I enter email as "<email>"
    And I enter phone number as "<phoneNbr>"
    And I click Continue on CC page
    And I check the agreed checkbox
    And I click Continue on CC review page
    Then I should see Congratulations page
    And I should see CC success message
    And I click Done on congratulations page

    Examples:
    |scenNbr| type | firstName | lastName |  creditCardNbr | expMonth | expYear | cvv | billingAddress | email | phoneNbr |
    | 1 | tax  |  John  |   Doe    |4111111111111111|    June   | 2026    | 123 | 111 Locust St| test@mailinator.com | 1231231234 |
#    | 2 | tax  |  John-Johny-Brian |   Doe-Doe-Doe    |4111111111111111|    June   | 2026    | 123 | 111 Locust St| test@mailinator.com | 1231231234 |
#    | 3 | tax  |  John Johny Brian |   Doe Doe Doe    |4111111111111111|    June   | 2026    | 123 | 111 Locust St| test@mailinator.com | 1231231234 |
#    | 4 | tax  |  John-Jr Johny Sr-Brian |   Doe-eey Doe-nat Doe    |4111111111111111|    June   | 2026    | 123 | 111 Locust St| test@mailinator.com | 1231231234 |
