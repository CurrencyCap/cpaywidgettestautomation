package pages;

import glueCode.driverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class WidgetHome {
    private WebDriver driver = driverManager.getWebdriver();

    private By buttonsDisplayed = new By.ByCssSelector(".payment-button");
    private By buttonCloseWidget = new By.ByClassName("header-close-button");
    private By titleWidget = new By.ByClassName("wizzard-body-title");
    private String textTitleWidget = "Choose a Payment Method";


    private boolean switchToWidgetFrame(){
        int size = driver.findElements(By.tagName("iframe")).size();

        for (int i =0; i<= size; i++){
            try{
                driver.switchTo().frame(i);
                if (driver.findElement(buttonCloseWidget) != null) break;
            }catch (Exception e){
                //System.out.println("");
            }
        }
        return driver.findElement(buttonCloseWidget).isDisplayed();
    }

    /**
     * @param option Credit; Bank; Payments
     * @return WebElement
     */
    private WebElement getPaymentOption(String option){
        List<WebElement> buttons = driver.findElements(buttonsDisplayed);
        for (WebElement e : buttons){
            if (e.getText().contains(option)) return e;
        }
        return null;
    }

    //check if widget is started, by trying to witch to the frame
    public boolean isWidgetStarted(){
        return switchToWidgetFrame();
    }

    public boolean isCreditCardOptionDisplayed(){
        if (getPaymentOption("Card") != null) return true;
        return false;
    }

    public boolean isACHOptionDisplayed(){
        if (getPaymentOption("Bank") != null) return true;
        return false;
    }

    public boolean isExpressFinancingOptionDisplayed(){
        if (getPaymentOption("Payments") != null) return true;
        return false;
    }

    public boolean clickCreditCardOption(){
        try{
            getPaymentOption("Card").click();
            return true;
        }catch (Exception e){
            System.out.println("\n Unable to click CreditCard Option.");
            return false;
        }

    }

    public boolean clickACHOption(){
        try{
            getPaymentOption("Bank").click();
            return true;
        }catch (Exception e){
            System.out.println("\n Unable to click ACH Option.");
            return false;
        }

    }
    public boolean clickExpressFinancingOption(){
        try{
            getPaymentOption("Payments").click();
            return true;
        }catch (Exception e){
            System.out.println("\n Unable to click ExpressFinancing Option.");
            return false;
        }

    }

}
