package pages;

import glueCode.driverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class WidgetCongratulations {
    private WebDriver driver = driverManager.getWebdriver();

    private By pageTitle = new By.ByClassName("title");
    private By pageSubtitle = new By.ByClassName("subtitle");
    private By buttonDone = new By.ById("next-label");

    public boolean verifyTitle(){
        return driver.findElement(pageTitle).getText().equalsIgnoreCase("congratulations!");
    }

    public boolean verifySubTitle(){
        return driver.findElement(pageSubtitle).getText().contains("Your payment has been sent to the merchant");
    }

    public boolean clickDoneButton(){
        if (driver.findElement(buttonDone).isEnabled()) {
            driver.findElement(buttonDone).click();
            return true;
        }
        return false;
    }
}
