package pages;

import glueCode.driverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;


public class WidgetCreditCardReview {
    private WebDriver driver = driverManager.getWebdriver();
    private By buttonContinue = new By.ById("next-label");
    private By buttonBack = new By.ById("back-label");
    private By checkboxAgree = new By.ByClassName("hide checkbox-helper");

    public boolean checkAgree(){
        if (!driver.findElement(checkboxAgree).isSelected()) {
            Actions build = new Actions(driver);
            build.moveToElement(driver.findElement(checkboxAgree), 5, 5).click().build().perform();
            return true;
        }
        return false;
    }

    public boolean uncheckAgree(){
        if (driver.findElement(checkboxAgree).isSelected()) {
            driver.findElement(checkboxAgree).click();
            return true;
        }
        return false;
    }

    public boolean clikcContinue(){
        if (driver.findElement(buttonContinue).isEnabled()){
            driver.findElement(buttonContinue).click();
            return true;
        }
        return false;
    }
}
