package pages;

import glueCode.driverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

public class WidgetCreditCard {
    private WebDriver driver = driverManager.getWebdriver();
    private String prefixCardData = "";
    private String prefixEmailPhone = "";
    private By inputFirstName;// = new By.ById(prefixCardData+"accountOwner_firstName");
    private By inputLastName;// = new By.ById(prefixCardData+"accountOwner_lastName");
    private By inputCardNumber;// = new By.ById(prefixCardData+"payment_number");
    private By dropDownExpMonth;// = new By.ById(prefixCardData+"payment_expDate_selectedMonth");
    private By dropDownExpYear;// = new By.ById(prefixCardData+"payment_expDate_selectedYear");
    private By inputCVV;// = new By.ById(prefixCardData+"payment_cvv");
    private By inputAddress;// = new By.ById(prefixCardData+"address");
    private By dropDownPredictions = new By.ByClassName("predictions");
    private By inputStreet = new By.ById("street");
    private By inputCity = new By.ById("city");
    private By inputState = new By.ById("state");
    private By inputZip = new By.ById("zip");
    private By inputEmail;// = new By.ById(prefixEmailPhone+"email");
    private By inputPhone;// = new By.ById(prefixEmailPhone+"phone");
    private By buttonContinue = new By.ById("next-label");
    private By buttonBack = new By.ById("back-label");

    private void getPrefix(){
        List<WebElement> allIDs = driver.findElements(new By.ByXPath("//*[@id]"));

        for (WebElement el : allIDs){
            if (el.getAttribute("id").contains("address")) {
                prefixCardData = el.getAttribute("id").split("address")[0];
                inputFirstName = new By.ById(prefixCardData+"accountOwner_firstName");
                inputLastName = new By.ById(prefixCardData+"accountOwner_lastName");
                inputCardNumber = new By.ById(prefixCardData+"payment_number");
                dropDownExpMonth = new By.ById(prefixCardData+"payment_expDate_selectedMonth");
                dropDownExpYear = new By.ById(prefixCardData+"payment_expDate_selectedYear");
                inputCVV = new By.ById(prefixCardData+"payment_cvv");
                inputAddress = new By.ById(prefixCardData+"address_");
                break;
            }
        }

        for (WebElement el : allIDs){
            if (el.getAttribute("id").contains("email")) {
                prefixEmailPhone = el.getAttribute("id").split("email")[0];
                inputEmail = new By.ById(prefixEmailPhone+"email");
                inputPhone = new By.ById(prefixEmailPhone+"phone");
                break;
            }
        }
    }

    private void initiateObjects(){
        if (prefixEmailPhone.length() == 0 || prefixCardData.length() == 0) getPrefix();
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    public boolean typeCardholderFirstName(String firstName){
        initiateObjects();
        driver.findElement(inputFirstName).sendKeys(firstName);
        return driver.findElement(inputFirstName).getAttribute("value").equalsIgnoreCase(firstName);
    }

    public boolean typeCardholderLastName(String lastName){
        initiateObjects();
        driver.findElement(inputLastName).sendKeys(lastName);
        return driver.findElement(inputLastName).getAttribute("value").equalsIgnoreCase(lastName);
    }

    public boolean typeCardNumber(String cardNumber){
        initiateObjects();
        driver.findElement(inputCardNumber).sendKeys(cardNumber);
        return driver.findElement(inputCardNumber).getAttribute("value").replaceAll(" ","").equalsIgnoreCase(cardNumber);
    }

    public boolean selectExpMonth(String month){
        initiateObjects();

        Select dropDown = new Select(driver.findElement(dropDownExpMonth));
        dropDown.selectByVisibleText(month);

        return dropDown.getOptions().get(Integer.parseInt(driver.findElement(dropDownExpMonth).getAttribute("value"))).getText().equalsIgnoreCase(month);
    }

    public boolean selectExpYear(String year){
        initiateObjects();
        (new Select(driver.findElement(dropDownExpYear))).selectByVisibleText(year);

        return driver.findElement(dropDownExpYear).getAttribute("value").equalsIgnoreCase(year);
    }

    public boolean typeCVV(String cvv){
        initiateObjects();
        driver.findElement(inputCVV).sendKeys(cvv);
        return driver.findElement(inputCVV).getAttribute("value").equalsIgnoreCase(cvv);
    }

    public boolean typeBillingAddress(String billingAddress){
        initiateObjects();
        driver.findElement(inputAddress).click();
        driver.findElement(inputAddress).sendKeys(billingAddress);

        List<WebElement> options = driver.findElement(dropDownPredictions).findElements(By.xpath(".//*"));
        for (WebElement option : options){
            System.out.println("\n Element text: [" + option.getText() + "]");
            if (option.getText().contains(billingAddress)) {
                System.out.println("Clicking on element: " +option.getText());
                option.click();
                break;
            }
        }
        return driver.findElement(inputAddress).getAttribute("value").contains(billingAddress);
    }

    public boolean typeBillingStreet(String billingStreet){
        initiateObjects();
        driver.findElement(inputStreet).sendKeys(billingStreet);
        return driver.findElement(inputStreet).getText().equalsIgnoreCase(billingStreet);
    }

    public boolean typeBillngCity(String billingCity){
        initiateObjects();
        driver.findElement(inputCity).sendKeys(billingCity);
        return driver.findElement(inputCity).getText().equalsIgnoreCase(billingCity);
    }

    public boolean typeBillingState(String billingState){
        initiateObjects();
        driver.findElement(inputState).sendKeys(billingState);
        return driver.findElement(inputState).getText().equalsIgnoreCase(billingState);
    }

    public boolean typeBillingZipCode(String billingZipCode){
        initiateObjects();
        driver.findElement(inputZip).sendKeys(billingZipCode);
        return driver.findElement(inputZip).getText().equalsIgnoreCase(billingZipCode);
    }

    public boolean typeEmailAddress(String emailAddress){
        initiateObjects();
        driver.findElement(inputEmail).sendKeys(emailAddress);
        return driver.findElement(inputEmail).getAttribute("value").equalsIgnoreCase(emailAddress);
    }

    public boolean typePhoneNumber(String phoneNumber){
        initiateObjects();
        driver.findElement(inputPhone).sendKeys(phoneNumber);
        return driver.findElement(inputPhone).getAttribute("value").replaceAll("\\)","").replaceAll("\\(","")
                .replaceAll("-","").replaceAll(" ","").equalsIgnoreCase(phoneNumber);
    }

    public boolean clickContinue(){
        initiateObjects();
        if (driver.findElement(buttonContinue).isEnabled()) {
            driver.findElement(buttonContinue).click();
            return true;
        }
        return false;
    }
}
