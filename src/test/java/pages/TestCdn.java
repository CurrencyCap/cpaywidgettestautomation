package pages;

import glueCode.driverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class TestCdn {
    private WebDriver driver = driverManager.getWebdriver();
    private String url = "http://test-cdn.currencypay.com/paywindow/1.1.0/index.html";

    private By textFieldMerchantID = new By.ById("inpMerchId");
    private By textFieldMerchantCode = new By.ById("inpMerchCode");
    private By buttonReInitializeWidget = new By.ById("initPayWindow");
    private By checkBoxCreditCard = new By.ById("slctCC1");
    private By checkBoxACH = new By.ById("slctACH");
    private By checkBoxExpressFinancing = new By.ById("slctECF");
    private By buttonBuyNowTax = new By.ByCssSelector("button.btn.btn-block.btn-lg.btn-warning");

//    private By checkBoxCreditCard = new By.ById("slctCC1");

    public boolean isTestCdnPage(){
        return driver.getCurrentUrl().equalsIgnoreCase(url);
    }

    public boolean goToTestCdn(){
        driver.get(url);
        return isTestCdnPage(); //TODO:update this to catch title - sometimes
    }

    //Unselect all options
    public boolean unselectAll(){
        try{
            unselectCreditCard();
            unselectACH();
            unselectExpressFinancing();
            return true;
        }catch (Exception e){
            System.out.println("Issues unselecting widget options [" + e.getMessage() + "]");
            return false;
        }
    }

    //CreditCard
    public boolean selectCreditCard(){
        if (!isCreditCardSelected())
            driver.findElement(checkBoxCreditCard).click();
        return true;
    }

    public boolean unselectCreditCard(){
        if (isCreditCardSelected())
            driver.findElement(checkBoxCreditCard).click();
        return true;
    }

    public boolean isCreditCardSelected(){
        return driver.findElement(checkBoxCreditCard).isSelected();
    }

    //ACH
    public boolean selectACH(){
        if (!isACHSelected())
            driver.findElement(checkBoxACH).click();
        return true;
    }

    public boolean unselectACH(){
        if (isACHSelected())
            driver.findElement(checkBoxACH).click();
        return true;
    }

    public boolean isACHSelected(){
        return driver.findElement(checkBoxACH).isSelected();
    }

    //Express Financing
    public boolean selectExpressFinancing(){
        if (!isExpressFinancingSelected())
            driver.findElement(checkBoxExpressFinancing).click();
        return true;
    }

    public boolean unselectExpressFinancing(){
        if (isExpressFinancingSelected())
            driver.findElement(checkBoxExpressFinancing).click();
        return true;
    }

    public boolean isExpressFinancingSelected(){
        return driver.findElement(checkBoxExpressFinancing).isSelected();
    }

    //Widget reInitialize TODO:Set as default; Reset Default
    public boolean reInitializeWidget(){
        try{
            driver.findElement(buttonReInitializeWidget).click();
            Thread.sleep(2000);
        }catch (Exception e){
            System.out.println("Some things happened while trying to click Re-Initialize Widget button + [" + e.getMessage() + "]");
            return false;
        }
        return true;
    }

    //Payment Options
    public boolean buyNowWith(String option){
//        if ("".equalsIgnoreCase(option)) {
//
//            return true;
//        }

        if ("tax".equalsIgnoreCase(option)) {
            driver.findElement(buttonBuyNowTax).click();
            return true;
        }

        System.out.println("The requested option [" + option + "] is not implemented!");
        return false;
    }

    //TODO: Manual Line Input

    //TODO: Shopping Cart
}
