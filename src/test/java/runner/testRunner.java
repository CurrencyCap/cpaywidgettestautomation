package runner;

import com.cucumber.listener.Reporter;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import glueCode.driverManager;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/features",
        glue = {"steps"},
        plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target/Cucumber-reports/report.html"},
        monochrome = true
)

public class testRunner extends driverManager {
    @Before
    public static void before(Scenario scenario){
        System.out.println("------------------------------");
        System.out.println("Starting - " + scenario.getName());
        System.out.println("------------------------------");
        System.out.println("this is where we would start the driver");
        driverManager.startWebdriver();
        driverManager.getWebdriver().manage().window().maximize();
    }

    @After
    public static void after(Scenario scenario){
        System.out.println("------------------------------");
        System.out.println(scenario.getName() + " Status - " + scenario.getStatus());
        System.out.println("------------------------------");
        driverManager.getWebdriver().quit();
    }

    @AfterClass
    public static void afterClass(Scenario scenario) {
        Reporter.loadXMLConfig(new File("config/report.xml"));
    }

}
