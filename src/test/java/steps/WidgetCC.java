package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import pages.WidgetCreditCard;

public class WidgetCC {
    public WidgetCreditCard widgetCreditCard = new WidgetCreditCard();

    @Then("^I enter first name as \"([^\"]*)\"$")
    public void iEnterFirstNameAs(String firstName){
        Assert.assertTrue("Unable to enter first name ["+firstName+"] into the field",widgetCreditCard.typeCardholderFirstName(firstName));
    }

    @And("^I enter last name as \"([^\"]*)\"$")
    public void iEnterLastNameAs(String lastName){
        Assert.assertTrue("Unable to enter last name ["+lastName+"] into the field",widgetCreditCard.typeCardholderLastName(lastName));
    }

    @And("^I enter card number as \"([^\"]*)\"$")
    public void iEnterCardNumberAs(String cardNumber){
        Assert.assertTrue("Unable to enter credit card number ["+cardNumber+"] into the field",widgetCreditCard.typeCardNumber(cardNumber));
    }

    @And("^I enter cvv as \"([^\"]*)\"$")
    public void iEnterCVVAs(String cvv){
        Assert.assertTrue("Unable to enter CVV ["+cvv+"] into the field",widgetCreditCard.typeCVV(cvv));
    }

    @And("^I enter email as \"([^\"]*)\"$")
    public void iEnterEmailAs(String email){
        Assert.assertTrue("Unable to enter email ["+email+"] into the field",widgetCreditCard.typeEmailAddress(email));
    }

    @And("^I enter phone number as \"([^\"]*)\"$")
    public void iEnterPhoneNumberAs(String phoneNumber){
        Assert.assertTrue("Unable to enter phone number ["+phoneNumber+"] into the field",widgetCreditCard.typePhoneNumber(phoneNumber));
    }

    @And("^I enter billing address as \"([^\"]*)\"$")
    public void iEnterBillingAddressAs(String address){
        Assert.assertTrue("Unable to find address ["+address+"]",widgetCreditCard.typeBillingAddress(address));
    }

    @And("^I select exp month as \"([^\"]*)\"$")
    public void iSelectExpMonthAs(String month){
        Assert.assertTrue("Unable to select month ["+month+"] from the drop-down",widgetCreditCard.selectExpMonth(month));
    }

    @And("^I select exp year as \"([^\"]*)\"$")
    public void iSelectExpYearhAs(String year){
        Assert.assertTrue("Unable to select year ["+year+"] from the drop-down",widgetCreditCard.selectExpYear(year));
    }

    @And("^I click Continue on CC page$")
    public void iClickContinueOnCCPage(){
        Assert.assertTrue("Unable to click Continue button",widgetCreditCard.clickContinue());
    }

    //"([^"]*)"

}
