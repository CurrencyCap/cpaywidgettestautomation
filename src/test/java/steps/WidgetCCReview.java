package steps;

import cucumber.api.java.en.And;
import org.junit.Assert;
import pages.WidgetCreditCardReview;

public class WidgetCCReview {
    WidgetCreditCardReview widgetCreditCardReview = new WidgetCreditCardReview();

    @And("^I check the agreed checkbox$")
    public void iCheckTheAgreedCheckbox(){
        Assert.assertTrue("Unable to click I agree checkbox",widgetCreditCardReview.checkAgree());
    }

    @And("^I click Continue on CC review page$")
    public void iClickContinueOnCCReviewPage(){
        Assert.assertTrue("Unable to click Continue button",widgetCreditCardReview.clikcContinue());
    }

}
