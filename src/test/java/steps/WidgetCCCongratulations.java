package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import pages.WidgetCongratulations;

public class WidgetCCCongratulations {
    WidgetCongratulations widgetCongratulations = new WidgetCongratulations();

    @Then("I should see Congratulations page")
    public void iShouldSeeCongratulationsPage(){
        Assert.assertTrue("Unable to verify Congratulations page",widgetCongratulations.verifyTitle());
    }

    @And("I should see CC success message")
    public void iShouldSeeCCSuccessMessage(){
        Assert.assertTrue("Unable to verify subTitle for Congratulations page",widgetCongratulations.verifySubTitle());
    }

    @And("I click Done on congratulations page")
    public void iClickDoneOnCongratulationsPage(){
        Assert.assertTrue("Unable to click Done button",widgetCongratulations.clickDoneButton());
    }

}
