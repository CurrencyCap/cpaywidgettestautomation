package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import pages.WidgetCreditCard;
import pages.WidgetHome;

public class WidgetPaymentOptions {
    public WidgetHome widgetHome = new WidgetHome();

    @Then("^I should see widget started with all payment options$")
    public void iShouldSeeWidgetStartedWithAllPaymentOptions(){
        Assert.assertTrue("Unable to find widget.",widgetHome.isWidgetStarted());
        Assert.assertTrue("Credit Card option is not displayed.",widgetHome.isCreditCardOptionDisplayed());
        Assert.assertTrue("ACH option is not displayed.",widgetHome.isACHOptionDisplayed());
        Assert.assertTrue("Express Financing option is not displayed.",widgetHome.isExpressFinancingOptionDisplayed());
    }

    @Then("^I should see widget started with ACH and ExpressFinancing$")
    public void iShouldSeeWidgetStartedWithACHAndExpressFinancing(){
        Assert.assertTrue("Unable to find widget.",widgetHome.isWidgetStarted());
        Assert.assertTrue("ACH option is not displayed.",widgetHome.isACHOptionDisplayed());
        Assert.assertTrue("Express Financing option is not displayed.",widgetHome.isExpressFinancingOptionDisplayed());
        Assert.assertFalse("Credit Card is still displayed.",widgetHome.isCreditCardOptionDisplayed());
    }

    @Then("^I should see widget started with CreditCard and ExpressFinancing$")
    public void iShouldSeeWidgetStartedWithCreditCardAndExpressFinancing(){
        Assert.assertTrue("Unable to find widget.",widgetHome.isWidgetStarted());
        Assert.assertTrue("Credit Card option is not displayed.",widgetHome.isCreditCardOptionDisplayed());
        Assert.assertTrue("Express Financing option is not displayed.",widgetHome.isExpressFinancingOptionDisplayed());
        Assert.assertFalse("ACH is still displayed.",widgetHome.isACHOptionDisplayed());
    }

    @And("^I click on CreditCard option$")
    public void iClickOnCreditCardOption(){
        Assert.assertTrue("Unable to click CreditCard option.",widgetHome.clickCreditCardOption());
    }

}
