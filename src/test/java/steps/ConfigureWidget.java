package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.TestCdn;
import pages.WidgetHome;

public class ConfigureWidget {
    public TestCdn testCdn = new TestCdn();
    public WidgetHome widgetHome = new WidgetHome();

    @Given("^I am on testCdn page$")
    public void iAmOnTestCdnPage(){
        Assert.assertTrue("Failed to navigate to TestCdn page.",testCdn.goToTestCdn());
    }

    @When("^I select CreditCard$")
    public void iSelectCreditCard(){
        Assert.assertTrue("Unable to select CreditCard checkbox.",testCdn.selectCreditCard());
    }

    @When("^I unselect CreditCard$")
    public void iUnselectCreditCard(){
        Assert.assertTrue("Unable to select CreditCard checkbox.",testCdn.unselectCreditCard());
    }

    @Then("^I should see CreditCard is unselected$")
    public void iShouldSeeCreditCardIsUnselected(){
        Assert.assertFalse("Credit Card checkbox is selected.",testCdn.isCreditCardSelected());
    }


    @Then("^I should see CreditCard is selected$")
    public void iShouldSeeCreditCardIsSelected(){
        Assert.assertTrue("Credit Card checkbox is unselected.",testCdn.isCreditCardSelected());
    }

    @Then("^I should see ExpressFinancing is selected$")
    public void iShouldSeeExpressFinancingIsSelected(){
        Assert.assertTrue("Credit Card checkbox is unselected.",testCdn.isExpressFinancingSelected());
    }

    @Then("^I should see ACH is selected$")
    public void iShouldSeeACHIsSelected(){
        Assert.assertTrue("ACH checkbox is unselected.",testCdn.isACHSelected());
    }

    @When("^I Re-Initialize Widget$")
    public void iReInitializeWidget(){
        Assert.assertTrue("Failed to reinitialize widget.",testCdn.reInitializeWidget());
    }

    @And("^I run with Buy Now \"([^\"]*)\"$")
    public void iRunWithBuyNow(String option){
        Assert.assertTrue("",testCdn.buyNowWith(option));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Assert.assertTrue("Unable to find widget.",widgetHome.isWidgetStarted());
    }

    @When("^I select CreditCard and ExpressFinancing$")
    public void iSelectCreditCardAndExpressFinancing(){
        testCdn.unselectAll();
        Assert.assertTrue("Unable to select CreditCard checkbox.",testCdn.selectCreditCard());
        Assert.assertTrue("Unable to select ExpressFinancing checkbox.",testCdn.selectExpressFinancing());
    }

    @When("^I select ACH and ExpressFinancing$")
    public void iSelectACHandExpressFinancing(){
        testCdn.unselectAll();
        Assert.assertTrue("Unable to select ACH checkbox.",testCdn.selectACH());
        Assert.assertTrue("Unable to select ExpressFinancing checkbox.",testCdn.selectExpressFinancing());
    }

    @When("^I select all payment options$")
    public void iSelectAllPaymentOptions(){
        testCdn.unselectAll();
        Assert.assertTrue("Unable to select CreditCard checkbox.",testCdn.selectCreditCard());
        Assert.assertTrue("Unable to select ExpressFinancing checkbox.",testCdn.selectExpressFinancing());
        Assert.assertTrue("Unable to select ACH checkbox.",testCdn.selectACH());
    }

    @Then("^I should see all payment options are selected$")
    public void iShouldSeeAllPaymentOptionsAreSelected(){
        iShouldSeeCreditCardIsSelected();
        iShouldSeeACHIsSelected();
        iShouldSeeExpressFinancingIsSelected();
    }

}
